import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        //Task1 c2f : на основании значения в градусах Цельсия вернуть значение в градусах Фаренгейта
        //System.out.println(c2f(2));

        // Task 2 m2i: на основании значения в метрах вернуть значение в дюймах
        //System.out.println(m2i(10));

        //Task3
        /*
         millisToFriendly: на основании значения в миллисекундах вернуть значение формата "mm+:ss.SSS",
         где mm+ - кол-во полных минут (n знаков),
         ss - количество секунд (2 знака), SSS - оставшихся миллисекунд (3 знака)
         */
        /*Date date = new Date();
        System.out.println(millisToFriendly(date.getTime()));*/

        //Task3
        // Второй вариант с использованием SimpleDateFormat;
        Date date = new Date();
        System.out.println(millisToFriendly2(date.getTime()));
    }

    public static double c2f(double celsius){
        return (celsius * 9/5) + 32;
    }

    public static double m2i(double meters){
        return meters * 39.37;
    }

    public static String millisToFriendly(long milliseconds){
        int millisecondsInSeconds = 1000;
        int millisecondsInMinutes = 60000;
        return String.format("Your time from 1 january 1970 years equals %d:%d:%d",
                milliseconds / millisecondsInMinutes, (milliseconds % millisecondsInMinutes) /
                        millisecondsInSeconds,milliseconds % millisecondsInSeconds);
    }

    public static String millisToFriendly2(long milliseconds){
        DateFormat dateFormat = new SimpleDateFormat("mm:ss:SS");
        return dateFormat.format(milliseconds);
    }
}
